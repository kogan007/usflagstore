const { createFilePath } = require(`gatsby-source-filesystem`)
const path = require(`path`)
const axios = require('axios');


exports.sourceNodes = async ({actions: {createNode}, createNodeId, createContentDigest}) => {


  const pageConfig = {
    method: 'get',
    url: `https://api.bigcommerce.com/stores/${process.env.STORE_HASH}/v2/pages`,
    headers: { 
      'Accept': 'application/json', 
      'Content-Type': 'application/json', 
      'X-Auth-Token': process.env.ACCESS_TOKEN
    },
  };

  const res2 = await axios(pageConfig);

  res2.data.forEach((page) => createNode({
    name: page.name,
    body: page.body,
    url: page.url,
    id: createNodeId(page.name),
    bcID: page.id,
    internal: {
      type: 'BCPage',
      contentDigest: createContentDigest(page)
    }
  }))



  async function getAllProducts() {
    // set some variables
    const productConfig = {
      headers: { 
        'Accept': 'application/json', 
        'Content-Type': 'application/json', 
        'X-Auth-Token': process.env.ACCESS_TOKEN
      }
    }

    const baseUrl = `${process.env.API_URL}/${process.env.STORE_HASH}/v3/catalog/products?page=`;
    let page = 1;

    let products = [];


    let lastResult = [];
    do {
      //loop over bigcommerce products to get all paths and is_visible boolean
      try {

        const result = await axios.get(`${baseUrl}${page}&limit=250&include=images`, productConfig);

        lastResult = result.data.data;
        result.data.data.forEach(product => {
          const {custom_url, name, is_visible, id, description, price, images} = product;
          
          products.push({custom_url, name, is_visible, id, description, price, images});
        });
 
        page++;
      } catch (err) {
        console.error(`Something went wrong ${err}`);
      }

    } while (lastResult.length !== 0);

    return products;
  }

  const products = await getAllProducts();
  products.forEach((product) => createNode({
    name: product.name,
    bcID: product.id,
    description: product.description,
    price: product.price,
    is_visible: product.is_visible,
    images: product.images,
    id: createNodeId(product.name),
    path: product.custom_url.url,
    internal: {
      type: 'BCProductPathTest',
      contentDigest: createContentDigest(product)
    }
  }))



  return;
};




exports.createPages = async ({ graphql, actions }) => {
    const { createPage } = actions
    const {data} = await graphql(`
    {
      allBcProductPathTest(filter: {is_visible: {eq: true}}) {
        nodes {
          path
          bcID
        }
      }
      allBcPage {
        nodes {
          name
          body
          url
          bcID
        }
      }
      bigcommerceData {
        site {
          products(first: 50) {
            edges {
              node {
                name
                id
                entityId
                path
              }
            }
          }
          categoryTree {
            name
            path
            entityId
            children {
              name
              path
              entityId
              children {
                name
                path
                entityId
              }
            }
          }
        }
      }
    }
    
  `);
    const products = data.allBcProductPathTest.nodes;
    const categories = data.bigcommerceData.site.categoryTree; 
    const pages = data.allBcPage.nodes;
    


    

    pages.forEach((customPage) => {
      createPage({
        path: `${customPage.url}`,
        component: path.resolve(`src/templates/page.js`),
        context: {
          page: customPage.bcID
        }
      });
    })

    categories.forEach((category) => {
      createPage({
        path: `${category.path}`,
        component: path.resolve(`src/templates/category-page.js`),
        context: {
          category: category.entityId,
          pathName: `${category.path}`
        }
      });
    });
    
    categories.forEach((category) => category.children.forEach((child) => {
      
      createPage({
        path: `${child.path}`,
        component: path.resolve(`src/templates/category-page.js`),
        context: {
          category: child.entityId,
          pathName: `${child.path}`
        }
      });
    }))
    


    products.forEach(product => {
        createPage({
          path: `/products${product.path}`,
          component: path.resolve(`src/templates/product-page.js`),
          context: {
            productId: product.bcID
          }
        });
      });

}





exports.onCreateNode = ({ node, actions, getNode }) => {
    const { createNodeField } = actions;
  
    if (node.internal.type === `MarkdownRemark`) {
      const value = createFilePath({ node, getNode });
      createNodeField({
        name: `slug`,
        node,
        value
      });
    }
  };