import React from 'react';
import { InstantSearch } from "react-instantsearch-dom"
import algoliasearch from "algoliasearch/lite"

export const GlobalStateContext = React.createContext()
export const GlobalDispatchContext = React.createContext()


const initialSearchState = { results: []};

const initialSubscribeStatus = { status: null }

const initialQuery = { query: ""}

export const queryReducer = (state, action) => {
  switch (action.type) {
    case "QUERY":
      return {
        ...state,
        query: action.payload
      }
  }
}

export const searchReducer = (state, action) => {
    switch (action.type){
        case "SEARCH":
            return { 
                ...state,
                results: action.payload
            }
        default: 
            return state;
    }
}

export const subscribeReducer = (state,action) => {
  switch (action.type) {
    case "UPDATE_STATUS":
      return {
        ...state,
        status: action.payload
      }
    default: 
      return state;
  }
}

const GlobalContextProvider = ({ children }) => {
    const [searchState, searchDispatch] = React.useReducer(searchReducer, initialSearchState)
    const [subscribeState, subscribeDispatch] = React.useReducer(subscribeReducer, initialSubscribeStatus)
    const [query, setQuery] = React.useReducer(queryReducer, initialQuery)


    const searchIndices = [{ name: `products`, title: `products` }]

    const searchClient = algoliasearch(
      process.env.GATSBY_ALGOLIA_APP_ID,
      process.env.GATSBY_ALGOLIA_SEARCH_KEY
    )

    return (
      <GlobalStateContext.Provider value={{searchState, subscribeState, query}}>
        <GlobalDispatchContext.Provider value={{searchDispatch, subscribeDispatch, setQuery}}>
        <InstantSearch
          searchClient={searchClient}
          indexName={searchIndices[0].name}
          onSearchStateChange={({ query }) => setQuery({
              type: "QUERY",
              payload: query
          })}
        >

          {children}
          </InstantSearch>
        </GlobalDispatchContext.Provider>
      </GlobalStateContext.Provider>
    )
  }


  export default GlobalContextProvider