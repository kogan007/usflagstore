/**
 * Layout component that queries for data
 * with Gatsby's useStaticQuery component
 *
 * See: https://www.gatsbyjs.com/docs/use-static-query/
 */

import React, {useReducer} from "react"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import Footer from './Footer';

import Header from "./Header"
import '../css/bootstrap.css'
import '../css/font-awesome.css'
import "./layout.scss"
import "@fontsource/bree-serif"



const Layout = ({ children }) => {



  const data = useStaticQuery(graphql`
    query {
      site {
        siteMetadata {
          title
        }
      }
    }
  `)

  return (
    
    <div className="container-fluid">
      <Header siteTitle={data.site.siteMetadata?.title || `Title`} />
      <div
      >
        <main className="site-content">{children}</main>

      </div>

      <Footer/>
    </div>
    
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
