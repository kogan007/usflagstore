import React, { useState, useEffect, useRef, useContext } from 'react'
import { useStaticQuery, graphql } from "gatsby"

import { navigate } from "gatsby"
import {Link} from 'gatsby';

import {
    GlobalDispatchContext,
    GlobalStateContext,
  } from "../../store"

const Search = () => {

    const {searchDispatch} = useContext(GlobalDispatchContext)
    const {searchState} = useContext(GlobalStateContext)


    
    const [query, setQuery] = useState('');
    const [query2, setQuery2] = useState('');

    
    

    

    const wrapperRef = useRef(null);
    useEffect(() => {

        function handleClickOutside(event) {
            if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
                setQuery("")
            }
        }

        document.addEventListener("mousedown", handleClickOutside);
        return () => {

            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [wrapperRef]);
    

    const handleChange = (event) => {
        setQuery(event.target.value)
        setQuery2(event.target.value)

        
	}
    const handleSubmit = (event) => {
        event.preventDefault();

        

        
       
    }
    return (
        <div ref={wrapperRef} className="search-wrap">

        <div className="form-group">
        <form onSubmit={handleSubmit}>
        <input type="text" onChange={handleChange} value={query2} className="form-control" placeholder="Search keyword or ID"/>
        <a rel="noreferrer" href="https://google.com" className="search-icon"><i className="fa fa-search"></i></a>
        </form>
        </div>

        <div className="search-preview">
            <ul>
            
            </ul>
        </div>
        </div>
    )
}

export default Search;