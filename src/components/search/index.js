import algoliasearch from "algoliasearch/lite"
import { createRef, default as React, useState, useContext } from "react"
import { InstantSearch } from "react-instantsearch-dom"

import SearchBox from './search-box'
import SearchResult from './search-result';

import useClickOutside from './useClickOutside';


import {GlobalStateContext, GlobalDispatchContext} from '../../store';

export default function Search({ indices }) {
  const rootRef = createRef()
  const {query: {query}} = useContext(GlobalStateContext);
  const {setQuery} = useContext(GlobalDispatchContext);


  const [hasFocus, setFocus] = useState(false)
  const searchClient = algoliasearch(
    process.env.GATSBY_ALGOLIA_APP_ID,
    process.env.GATSBY_ALGOLIA_SEARCH_KEY
  )

  useClickOutside(rootRef, () => setFocus(false))

  return (
      <div className="search-container" ref={rootRef}>
        
          <SearchBox className="form-group" onFocus={() => setFocus(true)} hasFocus={hasFocus} />
          <SearchResult
            className="search-preview"
            show={query && query.length > 0 && hasFocus}
            indices={indices}
          />
        
      </div>

  )
}