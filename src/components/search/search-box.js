import React, {useContext} from "react"
import { connectSearchBox } from "react-instantsearch-dom"
import {navigate} from 'gatsby'

import {GlobalStateContext, GlobalDispatchContext} from '../../store';



export default connectSearchBox(
  ({ refine, currentRefinement, className, onFocus }) => {
    const {query: {query}} = useContext(GlobalStateContext);
    const {setQuery} = useContext(GlobalDispatchContext);

    const handleSubmit = (event) => {
  
      event.preventDefault();
      setQuery({
        type: "QUERY",
        payload: event.target.value
      })

      
      navigate("/search-results")
    
    }

    const handleChange = (event) => {
      refine(event.target.value)
      setQuery({
        type: "QUERY",
        payload: event.target.value
      })
    }

    return (
    <form onSubmit={handleSubmit} className={className}>
      <input
        className="form-control"
        type="text"
        placeholder="Search"
        aria-label="Search"
        onChange={handleChange}
        value={query}
        onFocus={onFocus}
      />

    </form>
  )
    })