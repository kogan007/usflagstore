import React, {useState, useContext} from 'react';
import axios from 'axios';
import { navigate } from "gatsby"

import {
    GlobalDispatchContext,
    GlobalStateContext,
  } from "../store"

const SubscriptionForm = () => {
    const [email, setEmail] = useState("");

    const {subscribeDispatch} = useContext(GlobalDispatchContext)
    const {subscribeState} = useContext(GlobalStateContext)

    
	const handleChange = (event) => {
		setEmail(event.target.value)
	}

	const handleForm = async (event) => {
		event.preventDefault();

		try {
			const response = await axios.post("/.netlify/functions/bcSubscribe", {
				email
			});
			const {status} = response;
            
            subscribeDispatch({
                type: "UPDATE_STATUS",
                payload: status
            })
            
            navigate("/subscribe/")
			
		
		} catch(e) {
			const {response} = e;
			const {request, status, ...errorObject} = response;
            
            subscribeDispatch({
                type: "UPDATE_STATUS",
                payload: status
            })

			navigate(
				"/subscribe/"
			)
		}
    }
    
    return (
        <div className="newsletter-container">
                        <h3>Sign Up For Newsletter!</h3>
						<form onSubmit={handleForm} >
                        <div className="form-group">
                            <input type="text" onChange={handleChange} className="form-control" value={email} placeholder="Email Address" />
                            <button type="submit" className="subscribe-button">Subscribe</button>
                        </div>
						</form>
         </div>
    )
}


export default SubscriptionForm;
