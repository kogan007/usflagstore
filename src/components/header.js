
import PropTypes from "prop-types"
import React from "react"
import Image from "./Image"
import headerLogo from '../images/header-logo.png';
import cartIcon from '../images/site-shopping-cart.png';
import Nav from './nav';
import {Link} from 'gatsby'


import Search from "./search/"
const searchIndices = [{ name: `products`, title: `products` }]


const Header = ({ siteTitle }) => (
	<>
  <div className="row">
		<div className="top-bg-cover">
			<div className="custom-container">
				<div className="top-bar-container">
          <div className="left-header">
					<Link to="/" className="header-logo">
						<img src={headerLogo} className="img-fluid" alt=""/>
					</Link>
          </div>
					<div className="top-bar-right-sidebar">
						<div className="short-link-red-bar">
							<ul className="top-bar-links">
								<li><a href="tel:1-888-932-3524"><b>1-888-932-3524</b></a></li>
								<li><a href="https://google.com">Our History</a></li>
								<li><Link to="/contact-us">Contact Us</Link></li>
								<li><Link to="/login">My Account <img src={cartIcon} className="img-fluid shopping-cart-img" alt=""/></Link></li>
							</ul>	
						</div>
						<>
						<Search indices={searchIndices}/>
						
						</>
					</div>	
				</div>
			</div>	
		</div>
	</div>

	<Nav/>
	</>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
