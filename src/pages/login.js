import React, {useState} from "react"

import SEO from "../components/seo"
import Layout from "../components/Layout"

import axios from 'axios';

const LoginPage = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handlePassword = (event) => {
        setPassword(event.target.value);
    }
    const handleEmail = (event) =>{
        setEmail(event.target.value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();

        // post request to serverless function #TODO
    }

    return (
        <Layout>
            <SEO title="Login"/>


        <div className="row">
            <div className="col-lg-6">
                <form onSubmit={handleSubmit} className="form-group">
                    <input value={email} onChange={handleEmail} className="form-control" type="email"></input>
                    <input value={password} onChange={handlePassword} className="form-control" type="password"></input>

                    <button type="submit" className="btn default">Test</button>
                </form>
            </div>


            <div className="col-lg-6">

            </div>
        </div>
        </Layout>
    )
}


export default LoginPage;