import React, { useState, useEffect, useContext } from 'react';
import Layout from "../components/Layout"

import {
    GlobalDispatchContext,
    GlobalStateContext,
  } from "../store"

export default function Subscribe({location}){

    const {subscribeState: {status}} = useContext(GlobalStateContext)

    if (status) {
        return (
            <Layout>
                {status===200 ? (
                    <div>
                        Successfully Subscribed
                    </div>
                ): 
                (
                    <div>
                        Error
                    </div>
                )
            }
            </Layout>
        )
    }
    return (
        <Layout>
            Looking for something else?
        </Layout>
    )
    

}