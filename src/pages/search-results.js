import { default as React } from "react"
import Layout from "../components/Layout"
import {Link} from 'gatsby';
import {
  connectStateResults,
  Highlight,
  Hits,
  Index,
  Snippet,
  Pagination
} from "react-instantsearch-dom"


const HitCount = connectStateResults(({ searchResults }) => {

  const hitCount = searchResults && searchResults.nbHits

  return hitCount > 0 ? (
    <div className="HitCount">
      {hitCount} result{hitCount !== 1 ? `s` : ``}
    </div>
  ) : null
})

const renderImage = hit => {
  if (hit.images[0]){
    return hit.images[0].url_thumbnail;
  }
  return null;
}
const PageHit = ({ hit }) => {
  return (
  <div className="card" style={{ width: "18rem"}}>
  <img className="card-img-top" src={renderImage(hit)} alt="Card image cap"/>
  <div className="card-body">
    <h5 className="card-title">{hit.name}</h5>
    
    <Link to={`/products${hit.path}`} className="btn btn-primary">Go somewhere</Link>
  </div>
</div>
)}

const HitsInIndex = ({ index }) => (
  <Index indexName={index.name}>
    <HitCount />
    <Hits style={{display: "flex", flexWrap: "wrap"}} className="Hits" hitComponent={PageHit} />
  </Index>
)

const SearchResults = ({ className }) => {
  const indices = [{ name: `products`, title: `products` }]
  return (
    <Layout>
    <div className={className}>
        {indices.map(index => (
        <HitsInIndex index={index} key={index.name} />
        ))}

    </div>
    <Pagination />
    </Layout>
    )
}
export default SearchResults;