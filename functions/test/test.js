// for a full working demo of Netlify Identity + Functions, see https://netlify-gotrue-in-react.netlify.com/

const fetch = require('node-fetch')

const handler = async function (event, context) {
  if (!context.clientContext && !context.clientContext.identity) {
    return {
      statusCode: 500,
      // Could be a custom message or object i.e. JSON.stringify(err)
      body: JSON.stringify({
        msg: 'No identity instance detected. Did you enable it?',
      }),
    }
  }
  const { identity, user } = context.clientContext
  try {
    
    

    // Create the function for API Call 
    
    
    var requestOptions = {
      method: 'GET',
      headers: { 
        'X-Auth-Token': 'en4mnzagbzpu51zt9h9dmstoya0dklf', 
        'X-Auth-Client': 'bwixjfd5qtwb2wt2wlzag8puxtrsarm', 
        'Content-Type': 'application/json'
      }
    };
    let allData = [];

    async function getPages() {
      // set some variables
      const baseUrl = `https://api.bigcommerce.com/stores/lvtmj4gso4/v3/catalog/products?page=`;
      let page = 1;

      let products = [];
      // create a lastResult array which is going to be used to check if there is a next page
      let lastResult = [];
      do {
        // try catch to catch any errors in the async api call
        try {
          // use node-fetch to make api call
          const resp = await fetch(`${baseUrl}${page}`, requestOptions);
          const data = await resp.json();
          lastResult = data;

          data.data.forEach(product => {
            
            const {custom_url: {url}} = product;
            products.push(url);
          });
          // increment the page with 1 on each loop
          page++;
        } catch (err) {
          console.error(`Oeps, something is wrong ${err}`);
        }
        // keep running until there's no next page
      } while (lastResult.data !== null);
      // let's log out our new people array
      console.log(products);
    }
    
    
    await getPages();
    
    return {
      statusCode: 200,
      body: JSON.stringify({data: allData})
    }

  } catch (error) {
    // output to netlify function log
    console.log(error)
    return {
      statusCode: 500,
      // Could be a custom message or object i.e. JSON.stringify(err)
      body: JSON.stringify({ msg: error.message }),
    }
  }
}

module.exports = { handler }
