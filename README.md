## Get Started

1. **Download the zip file and extract it**

2. **Start cmd-line in the folder**
    ```cd folder-name```

3. **Install packages**
    ```npm i```

4. **Create file .env.local**
    <p>Email corey@makdigitaldesign.com for this file (contains bigcommerce api credentials)</p>

5. **Start up gatsby default / use netlify functions**
  ```gatsby develop```
  OR 
  ```netlify dev``` 

    <p>Newsletter subscribe wont work without netlify (must be ported to aws lambda in the future)</p>

6. **Go to localhost:8000 or localhost:8888 if using netlify dev**


## Notes 

1. **Search**
    <p>Search is currently being run through algolia, as it is well documented with react/gatsby projects. This can (and presumably will) be substituded for another service</p>

2. **Initial run**
    <p>There is a possibility that the initial build will fail, because there is an enormous amount of pages being generated (close to 1000) and the server times out. If this happens don't worry, just run</p>
    ```gatsby develop```
    or
    ```netlify dev```
    <p>again, and it should work. </p>

3. **State management**
    <p>Currently global state is being held in React's context API + the new react reducer hook. This can be moved to redux or some other state management tool if need be. 
    Let me know if you want any changes to this @ corey@makdigitaldesign.com</p>


## To do

1. **Componentize styles**

2. **Product Page and Category Page styles** 

2. **Auth**

3. **Cart** 
