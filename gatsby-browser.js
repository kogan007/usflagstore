import React from "react"
import GlobalContextProvider from "./src/store"

export const wrapRootElement = ({ element }) => {
  return <GlobalContextProvider>{element}</GlobalContextProvider>
}